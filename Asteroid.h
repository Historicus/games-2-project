// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _ASTEROID_H                 // Prevent multiple definitions if this 
#define _ASTEROID_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace AsteroidNS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                      // image height
	const int   TEXTURE_COLS = 4;           // texture has 8 columns
	//BARON actions
	const int ASTEROID_START = 44; //black asteroid 				
	const int ASTEROID_END = 44;

	
	const int ASTEROID_IDLE_START = 44;				
	const int ASTEROID_IDLE_END = 44;
	const float SPEED = 200.0f;                // 100 pixels per second
    const float MASS = 100.0f;         // image height
	const float ROTATION_RATE = (float)PI/2;

	const float ASTEROID_ANIMATION_DELAY = 0.035f;    // time between frames
	const float ASTEROID_IMAGE_SCALE = 1.1f;
}

// inherits from Entity class
class Asteroid : public Entity
{
private:
public:
    // constructor
    Asteroid();

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    //void damage(WEAPON);

	void setInvisible() {Image::setVisible(false); Entity::setActive(false);} //makes the enemy ship invisible
};
#endif

