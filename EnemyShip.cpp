// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "EnemyShip.h"

//=============================================================================
// default constructor
//=============================================================================
EnemyShip::EnemyShip() : Entity()
{
    spriteData.width = EnemyNS::WIDTH;           // size of Ship1
    spriteData.height = EnemyNS::HEIGHT;
    spriteData.rect.bottom = EnemyNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = EnemyNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = EnemyNS::ENEMY_ANIMATION_DELAY;
    startFrame = EnemyNS::ENEMY_IDLE_START;     // first frame of  animation
	endFrame     = EnemyNS::ENEMY_IDLE_END;     // last frame of  animation
	currentFrame = startFrame;
	radius = 17;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	//TODO: use this for damage
	//dead = false;
	mass = EnemyNS::MASS;
	collisionType = entityNS::CIRCLE;
	health = 5;
	//deadEnemy = false;

	timeInState = 0.0;
	srand (time(NULL));
	randomCooldown = 4; //between 3 and 16 seconds
	isDead = false;
	enemyShipDead = false;
}

//=============================================================================
// Initialize the .
// Post: returns true if successful, false if failed
//=============================================================================
bool EnemyShip::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    enemyDestroyed.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    enemyDestroyed.setFrames(EnemyNS::ENEMY_DESTROYED_START, EnemyNS::ENEMY_DESTROYED_END);
    enemyDestroyed.setCurrentFrame(EnemyNS::ENEMY_DESTROYED_START);
    enemyDestroyed.setFrameDelay(EnemyNS::ENEMY_ANIMATION_DELAY);
	enemyDestroyed.setLoop(false);

    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the 
//=============================================================================
void EnemyShip::draw()
{
    Image::draw();              // draw 
	//TODO: use this for damage
	if (!bulletsOnScreen.empty()){
		for (int i = 0; i < bulletsOnScreen.size(); i++){
			bulletsOnScreen[i].draw();
			//bulletsOnScreen.pop_back(); //take the bullet out of the array
		}
	}
	if (enemyShipDead)
		enemyDestroyed.draw(spriteData, 0);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void EnemyShip::update(float frameTime)
{
	Entity::update(frameTime);
	spriteData.angle = EnemyNS::ROTATION_RATE;  // rotate the 
	spriteData.x += frameTime * velocity.x;     // move  along X 
	spriteData.y += frameTime * velocity.y;     // move  along Y

	// Bounce off walls
	// if hit right screen edge
	if (spriteData.x > GAME_WIDTH-EnemyNS::WIDTH*getScale())
	{
		// position at right screen edge
		spriteData.x = GAME_WIDTH-EnemyNS::WIDTH*getScale();
		velocity.x = -velocity.x;               // reverse X direction
		//audio->playCue(MONSTER_BOUNCE);                  // play sound
	} 
	else if (spriteData.x < 0)                  // else if hit left screen edge
	{
		spriteData.x = 0;                       // position at left screen edge
		velocity.x = -velocity.x;               // reverse X direction
		//audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	// if hit bottom screen edge
	if (spriteData.y > GAME_HEIGHT-EnemyNS::HEIGHT*getScale())
	{
		// position at bottom screen edge
		spriteData.y = GAME_HEIGHT-EnemyNS::HEIGHT*getScale();
		velocity.y = -velocity.y;               // reverse Y direction
		//audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	else if (spriteData.y < 0)                  // else if hit top screen edge
	{
		spriteData.y = 0;                       // position at top screen edge
		velocity.y = -velocity.y;               // reverse Y direction
		//audio->playCue(MONSTER_BOUNCE);                  // play sound
	}

	timeInState += frameTime;
	if ((timeInState > randomCooldown) && (!isDead)){ //once frametime is >= our random time, it will fire. then a new random between 3 and 16 seconds will be calculated
		timeInState = 0; //reset the timer
		randomCooldown = rand()%12 + 3;
		fire();
	}

	//dying
	
	if (!bulletsOnScreen.empty()){
		for (int i = 0; i < bulletsOnScreen.size(); i++){
			bulletsOnScreen[i].update(frameTime);

			if (bulletsOnScreen[i].getY() >= GAME_HEIGHT + 100){ //off the bot of the screen
				//put the bullet back into the bulletVec vector
				/*bulletVec.push_back(bulletsOnScreen[i]);
				bulletsOnScreen.erase(bulletsOnScreen.begin() + i); */
				recycleBullet(i);

				/*bulletVec.back().setVisible(true); 
				bulletVec.back().setActive(true);*/
				//bulletVec.back().setVisible(true);
			}
		}
	}
	if(enemyShipDead)
	{
		enemyDestroyed.update(frameTime);
		if(enemyDestroyed.getAnimationComplete())
		{
			enemyShipDead = false;
			enemyDestroyed.setAnimationComplete(true);
			enemyDestroyed.setCurrentFrame(EnemyNS::ENEMY_DESTROYED_START);
			//playerDead  = true;
		}
	}
}

//=============================================================================
// damage
//=============================================================================
//void Enemy::damage(WEAPON weapon)
//{
//	health--;
//	if(health <= 0) {
//		dead = true;
//	}
//}

void EnemyShip::fire(){
	if (!bulletVec.empty()){
	bulletsOnScreen.push_back(bulletVec.back());
	//bulletsOnScreen.back().setVisible(true);
	bulletsOnScreen.back().setVisible(true); 
	bulletsOnScreen.back().setActive(true);

	bulletsOnScreen.back().setX(spriteData.x+12.5); //sets the bullets x coordinate to whereever the EnemyShip was when it fired
	bulletsOnScreen.back().setY(spriteData.y + 50); //sets the bullets y coordinate to whereever the EnemyShip was when it fired
	bulletVec.pop_back();
	
	audio->playCue(ENEMYFIRE);
	}
}

void EnemyShip::recycleBullet(int index){
	bulletVec.push_back(bulletsOnScreen[index]);
	bulletsOnScreen.erase(bulletsOnScreen.begin() + index); 
}

void EnemyShip::reset(float x, float y){
	isDead = false;
	this->enemyShipDead = false;
	this->setVisible(true); 
	this->setActive(true);
	this->setX(x);
	this->setY(y);
}